package testcollisions.boders;

import model.ball.Ball;
import model.ball.BallBuilder;
import model.ball.BallBuilderImpl;
import model.collision.*;
import element.Point2D;
import element.Point2DImpl;
import element.Vector2D;
import element.Vector2DImpl;
import org.junit.jupiter.api.Test;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

public class TestCollisionsWithBorders {
    private final BallBuilder builder = new BallBuilderImpl();
    CollisionManager collisionManagerCheck = new CollisionManagerImpl(null, 500, 350, 0, 0);

    @Test
    void testWithNoSpeedBall() {
        Point2D position = new Point2DImpl(1, 1);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(0, 0));

        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());
    }

    private Optional<CollisionDetectedImpl> topCollision(final Point2D newPos) {
        return Optional.of(new CollisionDetectedImpl(newPos));
    }

    //----------------------------------TOP------------------------------------

    @Test
    void testTopVerticalCollision() {
        int x = 5, y= 496;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(0, 1));
        //497
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //498
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //499
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //500
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, topCollision(new Point2DImpl(x,499)));
            if (collision.get().getNewCenterPosition().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), ball.getDirection());
            }
        }

        ball.setDirection(new Vector2DImpl(0, 1));
        ball.moveByDistance(2);
        //501
        collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, topCollision(new Point2DImpl(x,499)));
            if (collision.get().getNewCenterPosition().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), ball.getDirection());
            }
        }

        ball.setDirection(new Vector2DImpl(0, 1));
        ball.moveByDistance(3);
        //502
        collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, topCollision(new Point2DImpl(x,499)));
            if (collision.get().getNewCenterPosition().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), ball.getDirection());
            }
        }
    }

    @Test
    void testTopCollisionNotVerticalDirectionFromRight() {
        int x = 5, y= 496;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(1, 1));
        //497
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //497.7
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //498.4
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //499.1
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //499.8
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //500.5
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, topCollision(new Point2DImpl(8,499)));
            if (collision.get().getNewCenterPosition().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), ball.getDirection());
            }
        }
    }

    @Test
    void testTopCollisionNotVerticalDirectionFromLeft() {
        int x = 10, y= 496;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(-1, 1));
        //497
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //497.7
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //498.4
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //499.1
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //499.8
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //500.5
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, topCollision(new Point2DImpl(7,499)));
            if (collision.get().getNewCenterPosition().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), ball.getDirection());
            }
        }
    }



    //------------------------------------BOTTOM------------------------------------
    private Optional<CollisionDetectedImpl> generalBorderCollision(final Point2D newPos, final Vector2D newVec) {
        return Optional.of(new CollisionDetectedImpl(newVec.getNormalizedVector(), newPos));
    }

    @Test
    void testBottomVerticalCollision() {
        int x = 5;
        Point2D position = new Point2DImpl(x, 5);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(0, -1));
        //4
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //1
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(x,1), new Vector2DImpl(0, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }

        ball.moveByDistance(2);
        //-1
        collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(x,1), new Vector2DImpl(0, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }

        ball.setDirection(new Vector2DImpl(0, -1));
        ball.moveByDistance(3);
        //-2
        collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(x,1), new Vector2DImpl(0, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    @Test
    void testBottomCollisionNotVerticalDirectionFromLeft() {
        int x = 5;
        Point2D position = new Point2DImpl(x, 5);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(1, -1));
        //4
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //3.7
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //2.6
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //1.9
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //1.2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //0.5
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //-0.2
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(9,1), new Vector2DImpl(1, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    @Test
    void testBottomCollisionNotVerticalDirectionFromRight() {
        int x = 10;
        Point2D position = new Point2DImpl(x, 5);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(-1, -1));
        //4
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //3.7
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //2.6
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //1.9
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //1.2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //0.5
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //-0.2
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(6,1), new Vector2DImpl(-1, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    //------------------------------------LEFT------------------------------------

    @Test
    void testLeftHorizontalCollision() {
        int x = 347, y= 10;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(1, 0));
        //348
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //349
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //350
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(349,y), new Vector2DImpl(-1, 0)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }

        ball.moveByDistance(2);
        //351
        collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(349,y), new Vector2DImpl(-1, 0)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    @Test
    void testLeftCollisionNotHorizontalDirectionFromTop() {
        int x = 347, y= 15;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(1, -1));
        //348
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //348.7
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //349.4
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //350.1
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(349,13), new Vector2DImpl(-1, -1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    @Test
    void testLeftCollisionNotHorizontalDirectionFromBottom() {
        int x = 347, y= 15;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(1, 1));
        //348
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //348.7
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //349.4
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //350.1
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(349,17), new Vector2DImpl(-1, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    //-------------------------------------RIGHT------------------------------------

    @Test
    void testRightHorizontalCollision() {
        int x = 3, y= 10;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(-1, 0));
        //2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //1
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByDistance(1);
        //0
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(1,y), new Vector2DImpl(1, 0)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }

        ball.moveByDistance(2);
        //-1
        collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(1,y), new Vector2DImpl(1, 0)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    @Test
    void testRightCollisionNotHorizontalDirectionFromTop() {
        int x = 3, y= 15;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(-1, -1));
        //2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //1.3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //0.5
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //350.1
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(1,13), new Vector2DImpl(1, -1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }

    @Test
    void testRightCollisionNotHorizontalDirectionFromBottom() {
        int x = 3, y= 15;
        Point2D position = new Point2DImpl(x, y);
        Ball ball = builder.addStartPosition(position).addRadius(1).build();
        ball.setDirection(new Vector2DImpl(-1, 1));
        //2
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //1.3
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //0.5
        assertTrue(collisionManagerCheck.checkCollision(ball).isEmpty());

        ball.moveByTime(1);
        //350.1
        Optional<CollisionDetected> collision = collisionManagerCheck.checkCollision(ball);
        if (collision.isPresent()) {
            assertEquals(collision, generalBorderCollision(new Point2DImpl(1,17), new Vector2DImpl(1, 1)));
            if (collision.get().getNewCenterPosition().isPresent() && collision.get().getNewDirection().isPresent()) {
                ball.collision(collision.get().getNewCenterPosition().get(), collision.get().getNewDirection().get());
            }
        }
    }
}
