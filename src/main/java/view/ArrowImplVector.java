package view;

import element.Point2D;
import element.Point2DImpl;
import element.Vector2D;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.transform.Rotate;

/**
 * Implementation of the arrow that extends group.
 */
public class ArrowImplVector extends Group implements Arrow<Point2D, Vector2D> {
    /**
     * Length of the fixed arrow.
     */
    private static final int LENGTHF = 100;
    /**
     * Size of the triangle.
     */
    private static final int TRIANGLESIZE = 8;

    private final Line fixLine = new Line();
    private final Line dotLine = new Line();
    private final Polygon triangle = new Polygon();

    /**
     * The constructor of the ArrowImpl.
     */
    public ArrowImplVector() {
        super();
        this.getChildren().addAll(this.dotLine, this.fixLine, this.triangle);
        this.setVisible(false);
        this.fixLine.setStrokeWidth(4);
        this.dotLine.setStrokeLineCap(StrokeLineCap.ROUND);
    }

    /**
     * set the end of the arrow.
     *
     * @param vector the direction of the arrow.
     */
    @Override
    public void setEnd(final Vector2D vector) {
        if (!vector.isNullVector()) {
            final double distance = Math.sqrt(vector.getXComponent() * vector.getXComponent() + vector.getYComponent() * vector.getYComponent());
            setEndDotLine(vector, distance);
            setEndFixLine(vector, distance);
            setEndTriangle(vector.getDegreesAngle());
        }
    }

    /**
     * set the start of the arrow.
     *
     * @param start the starting point.
     */
    @Override
    public void setStart(final Point2D start) {
        setStartLine(this.dotLine, start);
        setStartLine(this.fixLine, start);
    }

    /**
     * Sets the color of the arrow based on the parameter.
     *
     * @param color the color of the arrow.
     */
    @Override
    public void setColor(final Color color) {
        if (!this.fixLine.getStroke().equals(color)) {
            this.fixLine.setStroke(color);
            this.dotLine.setStroke(color);
            this.triangle.setStroke(color);
            this.triangle.setFill(color);
        }
    }

    private void setStartLine(final Line line, final Point2D startP) {
        line.setStartX(startP.getX());
        line.setStartY(startP.getY());
    }

    private void setEndLine(final Line line, final Point2D endP) {
        line.setEndX(endP.getX());
        line.setEndY(endP.getY());
    }

    private void setEndFixLine(final Vector2D vector, final double distance) {
        final Point2D fine = calcEndPoint(LENGTHF, vector, distance);
        setEndLine(this.fixLine, fine);
    }

    private void setEndDotLine(final Vector2D vector, final double distance) {
        final Point2D fine = calcEndPoint(distance * 3 + LENGTHF, vector, distance);
        this.dotLine.getStrokeDashArray().setAll(2d, distance / 3);
        setEndLine(this.dotLine, fine);
    }

    private void setEndTriangle(final double angle) {
        this.triangle.getPoints().setAll(this.fixLine.getEndX() + TRIANGLESIZE, this.fixLine.getEndY(), this.fixLine.getEndX() - TRIANGLESIZE, this.fixLine.getEndY(), this.fixLine.getEndX(), this.fixLine.getEndY() - TRIANGLESIZE * 2);
        this.triangle.getTransforms().setAll(new Rotate(angle + 90, this.fixLine.getEndX(), this.fixLine.getEndY()));
    }

    private Point2D calcEndPoint(final double length, final Vector2D vector, final double distance) {
        final double ratio = length / distance;
        return new Point2DImpl(this.dotLine.getStartX() + ratio * vector.getXComponent(), this.dotLine.getStartY() + ratio * vector.getYComponent());
    }

}



