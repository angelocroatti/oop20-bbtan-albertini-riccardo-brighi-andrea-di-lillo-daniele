package view.bonus;

import element.Point2D;
import model.bonusblock.BonusBlock;

/**
 * This class represent the graphic bonus.
 */
public interface BonusView {

    /**
     * @return the radius of the bonus.
     */
    double getRadius();

    /**
     * @return the position of the ball.
     */
    Point2D getPosition();

    /**
     * Set the position of the bonus.
     *
     * @param newPos the new position.
     */
    void setPosition(Point2D newPos);

    /**
     * @return the logic bonus representing the BonusView.
     */
    BonusBlock getLogicBonus();

}
