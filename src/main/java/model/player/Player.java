package model.player;

import utility.BallColor;

import java.util.Set;

/**
 * This interface model the player in its own properties.
 */
public interface Player {

    /**
     * @return Return the list of the unlocked colors.
     */
    Set<BallColor> getColors();

    /**
     * @return Return the current amount of money hold by the player.
     */
    int getCurrentMoney();

    /**
     * @return Return the best score of the player.
     */
    int getBestScore();

    /**
     * @return Return the name of the player.
     */
    String getName();

    /**
     * @param money Money to be added to the player wallet.
     */
    void addMoney(int money);

    /**
     * @param money Money to be taken from the users wallet for some shop action.
     */
    void useMoney(int money);

    /**
     * @param newScore The new score to be saved/overwritten if greater than the current best score.
     */
    void setNewScore(int newScore);

    /**
     * @return Return the last score performed by the player.
     */
    int getLastScore();

    /**
     * @param radius This is the radius to be set as current radius used.
     */
    void modifyCurrentBallRadius(int radius);

    /**
     * @param newColor This is the new color to be added in the unlocked colors collection.
     */
    void addNewColor(BallColor newColor);

    /**
     * @return Return the current color used by the player.
     */
    BallColor getCurrentColor();

    /**
     * @param color Set the current color.
     */
    void setCurrentColor(BallColor color);

    /**
     * @return Return the current ball radius used by the player.
     */
    Integer getCurrentBallRadius();

    /**
     * @return Return the ball damage unlocked by the player.
     */
    Integer getCurrentBallDamage();

    /**
     * @return Return the unlocked radius set.
     */
    Set<Integer> getUnlockedRadius();

    /**
     * @param radius Add new radius to the unlocked radius set.
     */
    void addUnlockedRadius(int radius);

    /**
     * This method increment damage amount unlocked by the player.
     */
    void incrementDamage();

    /**
     * @param price Is the price of some product to buy.
     * @return Return true if player has money greater or equal than price, false otherwise.
     */
    boolean hasEnoughMoney(int price);
}
