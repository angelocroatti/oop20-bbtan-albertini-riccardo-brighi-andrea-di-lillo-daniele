package controller.viewcontroller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import model.player.Player;
import model.player.PlayersCollection;
import model.player.PlayersCollectionImpl;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;
import utility.BallColor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * The controller used for the Shop.fxml.
 */
public class ShopController implements Initializable {

    private static final int COSTRADIUS = 5;
    private static final int COSTCOLOR = 2;
    private static final int COSTDMG = 5;
    private static final int MAXRADIUS = 13;
    private static final int MINRADIUS = 5;

    @FXML
    private Button btnDanni;
    @FXML
    private Button btnColor;
    @FXML
    private Label lblMoney;
    @FXML
    private Pane rootPane;
    @FXML
    private Pane pane;
    @FXML
    private Label lblDmg;
    @FXML
    private Circle circleColor;
    @FXML
    private HBox boxRadius;
    @FXML
    private ChoiceBox<BallColor> boxColor;

    private Player p;
    private PlayersCollection pC;

    /**
     * initialize the controller
     *
     * @param location  the location.
     * @param resources contains all the fxml things.
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        try {
            pC = PlayersCollectionImpl.getCollectionFromDisk();
        } catch (IOException | ParseException e) {
            Logger.error(e);
        }
        pC.getCurrentPlayer().ifPresentOrElse(
                player -> {
                    p = player;
                    lblMoney.setText(Integer.toString(player.getCurrentMoney()));
                    lblDmg.setText(p.getCurrentBallDamage().toString());
                    btnDanni.setText(btnColor.getText() + "  " + COSTDMG);
                    btnColor.setText(btnColor.getText() + "  " + COSTCOLOR);
                    updateRadius(player.getUnlockedRadius());
                    updateColor();
                },
                () -> {
                    pane.getChildren().clear();
                    final BorderPane borderPane = createBrdPane();
                    final Label b = createLabel("PER ACCEDERE ALLO SHOP BISOGNA \n PRIMA EFFETTUARE IL LOGIN");
                    final ImageView img = new ImageView();
                    img.setImage(new Image(String.valueOf(ClassLoader.getSystemResource("images/Login-button.png"))));
                    img.setOnMouseClicked(e -> {
                        try {
                            pane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/playerInsertionPage.fxml"));
                            rootPane.getChildren().setAll(pane);
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    });
                    borderPane.setTop(b);
                    borderPane.setCenter(img);
                    pane.getChildren().add(borderPane);
                }
        );
    }

    private void updateRadius(final Set<Integer> set) {
        IntStream.range(MINRADIUS, MAXRADIUS).
                filter(v -> !set.contains(v)).
                forEach(v -> {
                    final BorderPane brdPane = createBrdPane();
                    final Label lbl = createLabel(Integer.toString(v));
                    brdPane.setTop(lbl);
                    brdPane.setBottom(createBtn(v));
                    brdPane.setCenter(createCircle(v));
                    BorderPane.setAlignment(lbl, Pos.CENTER);

                    boxRadius.getChildren().add(brdPane);
                });
    }

    private void updateColor() {
        boxColor.getItems().addAll(Stream.of(BallColor.values()).
                filter(c -> !p.getColors().contains(c)).
                collect(Collectors.toSet()));

        boxColor.getSelectionModel().selectFirst();
    }


    private boolean buying(final int cost) {
        if (p.hasEnoughMoney(cost)) {
            p.useMoney(cost);
            lblMoney.setText(Integer.toString(p.getCurrentMoney()));
            return true;
        }
        return false;
    }

    private Button createBtn(final int a) {
        final Button btn = new Button(Integer.toString(COSTRADIUS));
        btn.setMaxWidth(Double.MAX_VALUE);
        btn.setOnAction(b -> {
            if (buying(COSTRADIUS)) {
                p.addUnlockedRadius(a);
                boxRadius.getChildren().remove(btn.getParent());
            }
        });
        return btn;
    }

    private BorderPane createBrdPane() {
        final BorderPane borderPane = new BorderPane();
        borderPane.getStyleClass().add("borderPane_style");
        borderPane.setPadding(new Insets(4, 4, 3, 4));
        return borderPane;
    }

    private Label createLabel(final String text) {
        final Label lbl = new Label(text);
        lbl.setTextFill(Color.WHITE);
        lbl.getStyleClass().add("scrollpane-text");
        return lbl;
    }

    private Circle createCircle(final int radius) {
        final Circle c = new Circle(radius * 2.5);
        c.setFill(Color.WHITE);
        return c;
    }

    /**
     * When the ChoiceBox value changes, change the color of the circle.
     */
    @FXML
    public void onChangeColor() {
        if (!boxColor.getItems().isEmpty()) {
            circleColor.setFill(boxColor.getValue().getColor());

        } else {
            circleColor.setFill(BallColor.WHITE.getColor());
        }
    }

    /**
     * When the btn is pressed, buy the color, if you have money.
     */
    @FXML
    public void btnBuyColor() {
        if (buying(COSTCOLOR) && !boxColor.getItems().isEmpty()) {
            p.addNewColor(boxColor.getValue());
            boxColor.getItems().remove(boxColor.getValue());
            boxColor.getSelectionModel().selectFirst();
        }
    }

    /**
     * Increment the damage of the ball.
     */
    @FXML
    public void btnBuyDmg() {
        if (buying(COSTDMG)) {
            p.incrementDamage();
            lblDmg.setText(p.getCurrentBallDamage().toString());
        }
    }

    /**
     * Return to the mainPane.
     *
     * @throws IOException if it's not found.
     */
    @FXML
    public void btnHome() throws IOException {
        pC.update();
        final Pane mainPane = FXMLLoader.load(ClassLoader.getSystemResource("layouts/main.fxml"));
        rootPane.getChildren().setAll(mainPane);
    }

}
